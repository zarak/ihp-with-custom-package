module Web.Controller.Static where
import Web.Controller.Prelude
import Web.View.Static.Welcome
import MyLib (someFunc)

instance Controller StaticController where
    action WelcomeAction = do
        someFunc
        render WelcomeView
