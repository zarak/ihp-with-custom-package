{ mkDerivation, base, lib, customPackageDir }:
mkDerivation {
  pname = "my-awesome-project";
  version = "0.1.0.0";
  src = "${toString customPackageDir}/my-awesome-project";
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [ base ];
  executableHaskellDepends = [ base ];
  license = "unknown";
  hydraPlatforms = lib.platforms.none;
}
