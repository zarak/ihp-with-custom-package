# See https://ihp.digitallyinduced.com/Guide/package-management.html
{ ihp }:
import "${toString ihp}/NixSupport/make-nixpkgs-from-options.nix" {
    ihp = ihp;
    haskellPackagesDir = ./haskell-packages/.;
    manualOverrides = haskellPackagesNew: haskellPackagesOld: rec { 
        my-awesome-project = haskellPackagesNew.callPackage
        ./haskell-packages/my-awesome-project.nix { customPackageDir = "${./.}"; };
    };
}
