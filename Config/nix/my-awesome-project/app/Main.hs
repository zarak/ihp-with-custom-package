module Main where

import qualified MyLib (someFunc, someOtherFunc)

main :: IO ()
main = do
  putStrLn MyLib.someOtherFunc
  putStrLn "Hello, Haskell!"
  MyLib.someFunc
