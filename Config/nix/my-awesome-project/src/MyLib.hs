module MyLib (someFunc, someOtherFunc) where

someFunc :: IO ()
someFunc = putStrLn "someFunc"

someOtherFunc = "someOtherFunc"
