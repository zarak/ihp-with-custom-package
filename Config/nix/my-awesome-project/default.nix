let
  pkgs = import <nixpkgs> { };
in
  pkgs.haskellPackages.callPackage ./my-awesome-project.nix { }
